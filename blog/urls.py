from django.contrib.auth.decorators import login_required
from django.urls import path, include
from . import views


urlpatterns = [
    path('signup/', login_required(views.SignUp.as_view()), name='signup'),
]