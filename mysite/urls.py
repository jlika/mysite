from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic.base import TemplateView
from django.conf import settings
from django.conf.urls.static import static

import customer.urls

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('login/', include('django.contrib.auth.urls')),
                  path('signup/', include('django.contrib.auth.urls')),
                  path('', login_required(TemplateView.as_view(template_name='home.html')), name='home'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += customer.urls.urlpatterns
