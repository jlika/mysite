# Generated by Django 2.1.5 on 2019-01-23 12:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=20, verbose_name='First Name')),
                ('last_name', models.CharField(max_length=20, verbose_name='Last Name')),
                ('email', models.EmailField(max_length=20, verbose_name='Email')),
                ('phone_number', models.CharField(max_length=20, verbose_name='Phone')),
                ('address', models.CharField(max_length=20, verbose_name='Address')),
                ('work', models.CharField(max_length=20, verbose_name='Work')),
                ('position', models.CharField(max_length=20, verbose_name='Position')),
            ],
        ),
        migrations.AddField(
            model_name='customer',
            name='address',
            field=models.CharField(blank=True, max_length=20, verbose_name='Address'),
        ),
        migrations.AddField(
            model_name='customer',
            name='position',
            field=models.CharField(blank=True, max_length=20, verbose_name='Position'),
        ),
        migrations.AddField(
            model_name='customer',
            name='work',
            field=models.CharField(blank=True, max_length=20, verbose_name='Work'),
        ),
    ]
