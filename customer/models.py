from django.db import models


class Customer(models.Model):
    class Meta:
        verbose_name = 'Klienti'
        verbose_name_plural = 'Klientet'
        db_table = 'customer'

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(max_length=20)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=20, blank=True)
    work = models.CharField(max_length=20, blank=True)
    position = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
