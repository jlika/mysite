from django.contrib.auth.decorators import login_required
from django.urls import path, include
from customer.views import CustomerListView, CustomerCreateView

customer_urlpatterns = ([
                            path('list/', login_required(CustomerListView.as_view()), name="list"),
                            path('add/', login_required(CustomerCreateView.as_view()), name="add"),
                        ],
                        'customer')

urlpatterns = [
    path('customer/', include(customer_urlpatterns)),

]
