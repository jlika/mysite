from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView

from customer.forms import CustomerCreationForm
from customer.models import Customer


class CustomerListView(TemplateView):
    model = Customer
    template_name = "customer/list.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerListView, self).get_context_data(**kwargs)
        context['customers'] = Customer.objects.all()
        return context


class CustomerCreateView(CreateView):
    template_name = "customer/form.html"
    model = Customer
    form_class = CustomerCreationForm
    success_url = reverse_lazy('customer:list')

    def get_context_data(self, **kwargs):
        context = super(CustomerCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))
